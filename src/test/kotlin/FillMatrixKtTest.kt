import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.assertEquals

internal class FillMatrixKtTest {

        @Test
        fun fillMatrix5x5() {       //odd
            val matrix = fillMatrix(5)
            assertEquals(1, matrix[0][0])
            assertEquals(2, matrix[1][0])
            assertEquals(3, matrix[2][0])
            assertEquals(4, matrix[3][0])
            assertEquals(5, matrix[4][0])
            assertEquals(6, matrix[4][1])
            assertEquals(7, matrix[4][2])
            assertEquals(8, matrix[4][3])
            assertEquals(9, matrix[4][4])
            assertEquals(10, matrix[3][4])
            assertEquals(11, matrix[2][4])
            assertEquals(12, matrix[1][4])
            assertEquals(13, matrix[0][4])
            assertEquals(14, matrix[0][3])
            assertEquals(15, matrix[1][3])
            assertEquals(16, matrix[2][3])
            assertEquals(17, matrix[3][3])
            assertEquals(18, matrix[3][2])
            assertEquals(19, matrix[3][1])
            assertEquals(20, matrix[2][1])
            assertEquals(21, matrix[1][1])
            assertEquals(22, matrix[0][1])
            assertEquals(23, matrix[0][2])
            assertEquals(24, matrix[1][2])
            assertEquals(25, matrix[2][2])
        }

        @Test
        fun fillMatrix4x4() {     //even
            val matrix = fillMatrix(4)
            assertEquals(1, matrix[0][0])
            assertEquals(2, matrix[1][0])
            assertEquals(3, matrix[2][0])
            assertEquals(4, matrix[3][0])
            assertEquals(5, matrix[3][1])
            assertEquals(6, matrix[3][2])
            assertEquals(7, matrix[3][3])
            assertEquals(8, matrix[2][3])
            assertEquals(9, matrix[1][3])
            assertEquals(10, matrix[0][3])
            assertEquals(11, matrix[0][2])
            assertEquals(12, matrix[1][2])
            assertEquals(13, matrix[2][2])
            assertEquals(14, matrix[2][1])
            assertEquals(15, matrix[1][1])
            assertEquals(16, matrix[0][1])
        }
}