fun fillMatrix(Dimension: Int):Array<Array<Int>>
{
    val thisArray: Array<Array<Int>> = Array(Dimension) { Array(Dimension) { 0 } }

    var row = 0
    var column = 0

    var number = 1

    var right = true                        //Reversal variable

    var limiter = 1                         //vertical Limiter,to not going into already created line

    var leftLimiter = 1                     //horizontal limiter for the left edge
    var rightLimiter = Dimension            //horizontal limiter for the right edge


    thisArray[row][column] = number         //Initializing the first number
    number++

    while(number != Dimension*Dimension)        //can be just TRUE, but it is written like this for readability
    {
        //Down a few steps
        for(i in limiter until Dimension)
        {
            row++
            thisArray[row][column] = number
            number++
        }



        if(number >= Dimension*Dimension)     //check - did we go through all the numbers. This is one of two places
            break                             //where the array may already be completely reinitialized.


        //right-left a few steps
        for(i in leftLimiter until rightLimiter)
        {
            if(right) {                             //going to the right or to the left
                column++
            } else {
                column--
            }
            thisArray[row][column] = number
            number++
        }


        //Up a few steps
        for(i in limiter until Dimension)
        {
            row--
            thisArray[row][column] = number
            number++
        }


        if(number >= Dimension*Dimension)           //Second place after steps Up.
            break


        //step left-right once
        if(right) {
            column--
        } else {
            column++
        }
        thisArray[row][column] = number
        number++


        //change direction
        right = !right

        //change limiters
        limiter++
        leftLimiter++
        rightLimiter--
    }
    return thisArray
}